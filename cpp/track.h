#ifndef TRACK
#define TRACK

#include <map>
#include <cmath>

class TrackPiece
{
public:
	TrackPiece(const float pieceLength, const float angle=0, const bool laneSwitch=false) :
	length(pieceLength), 
	angle(angle),
	laneSwitch(laneSwitch)
	
	{if (angle==0)
		this->straight=true;
	else
		this->straight=false;
	}
		
		
	float getLength()
		{if (!straight)
			{float len = angle*2*3.14/360*length;
			if(len < 1)
				len*=-1;
			return len;}
		return length;}

	float getRadius()
		{return length;}
	bool isSwitch()
		{return laneSwitch;}
	float getAngle()
		{return angle;}
	float getTarget(float laneOffset)
		{float rad=length;
		if (angle>0)
			{rad-=laneOffset;}
		else
			{rad+=laneOffset;}		
		float target=sqrt(rad/90.0);
		if (angle < 60.0)
			target *= (60.0/angle);
		if (target < 0)
			target*=-1;
		return target;}

	bool isStraight()
		{return straight;}
	
private:
	float length;
	float angle;
	bool laneSwitch;
	bool straight;
	

};

#endif