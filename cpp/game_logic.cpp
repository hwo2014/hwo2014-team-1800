#include "game_logic.h"
#include "protocol.h"


using namespace hwo_protocol;

game_logic::game_logic()
  : action_map
    {
      { "join", &game_logic::on_join },
      { "yourCar", &game_logic::on_yourcar },
	  { "gameInit", &game_logic::on_game_init},
      { "gameStart", &game_logic::on_game_start },
      { "carPositions", &game_logic::on_car_positions },
      { "crash", &game_logic::on_crash },
      { "spawn", &game_logic::on_spawn },
      { "gameEnd", &game_logic::on_game_end },
      { "error", &game_logic::on_error },
	  { "tournamentEnd", &game_logic::on_tournament_end },
	  { "turboAvailable", &game_logic::on_turbo }
    },
	allowSwitch(true),
	turbo(false),
	currentIndex(0),
	currentPos(0),
	cornerAngle(0.0),
	angle(0),
	crashVel(6.6),
	crashAngle(60.0),
	started(false),
	tested(false),
	brakeTestVel(0.0),
	deceleration(1.8)
{
}

game_logic::msg_vector game_logic::react(const jsoncons::json& msg)
{
  const auto& msg_type = msg["msgType"].as<std::string>();
  const auto& data = msg["data"];
  auto action_it = action_map.find(msg_type);
  if (action_it != action_map.end())
  {
    return (action_it->second)(this, data);
  }
  else
  {
    std::cout << "Unknown message type: " << msg_type << std::endl;
    return { make_ping() };
  }
}

game_logic::msg_vector game_logic::on_join(const jsoncons::json& data)
{
  std::cout << "Joined" << std::endl;
  return { make_ping() };
}

game_logic::msg_vector game_logic::on_yourcar(const jsoncons::json& data)
{
  std::cout << "Car information received" << std::endl;
  color = data["color"].as<std::string>();
  return { make_ping() };
}

game_logic::msg_vector game_logic::on_game_init(const jsoncons::json& data)
{
	std::cout << "Track information received" << std::endl;
	const jsoncons::json& race=data["race"];
	const jsoncons::json& track=race["track"];
	const jsoncons::json& laneInfo=track["lanes"];
	const jsoncons::json& cars=race["cars"];
	bool laneSwitch;
	float length;
	float angle;
	pieceCount = track["pieces"].size();
	for (size_t piece=0; piece!= pieceCount; ++piece)
		{if (track["pieces"][piece].has_member("switch")==1)
			laneSwitch = true;
		else
			laneSwitch = false;
			
		if (track["pieces"][piece].has_member("length")==1)
			{length=track["pieces"][piece]["length"].as<double>();
			angle=0;}
			
		if (track["pieces"][piece].has_member("radius")==1)
			{length=track["pieces"][piece]["radius"].as<double>();
			angle=track["pieces"][piece]["angle"].as<double>();}
			
		pieces[piece]= new TrackPiece(length, angle, laneSwitch);
// Track printing
//		std::cout <<piece <<  " l " << length << " a " << angle << std::endl;
		}
	pieces[pieceCount]= new TrackPiece(pieces[0]->getLength(),pieces[0]->getAngle(), pieces[0]->isSwitch()); 
	
	float rad;
	float ang;
	float next;
	size_t i=0;
	size_t start=0;
	while (i<pieceCount)
		{while (i<pieceCount && pieces[i]->isStraight())
			i++;
		if (i==pieceCount)
			break;
		start=i;
		rad=pieces[i]->getRadius();
		ang=pieces[i]->getAngle();
		i++;
		next=pieces[i]->getAngle();
		while ((pieces[i]->getRadius()==rad) && ((ang>0 && next > 0) || (ang<0 && next<0)))
			{if (pieces[i]->isStraight())
				break;
			//std::cout << pieces[i]->getAngle() << " ";
			ang+=next;
			next=pieces[i+1]->getAngle();
			i++;}
		//std::cout<<start << std::endl;
		if (!pieces[start]->isStraight())
			corners[start]= new TrackPiece(rad, ang);}

//	for (std::map<size_t, TrackPiece*>::iterator it=corners.begin(); it!= corners.end(); it++)
//		std::cout<< it->first << " " << it->second->getAngle() << " " << it->second->getRadius() << std::endl;
	laneCount = laneInfo.size();
	size_t index;
	float dist;
	for (size_t lane=0; lane!=laneCount; lane++)
		{index=laneInfo[lane]["index"].as<size_t>();
		dist=laneInfo[lane]["distanceFromCenter"].as<double>();
		lanes[index]=dist;}
	
	size_t carCount=cars.size();
	for (size_t index=0; index!=carCount; index++)
		{if(cars[index]["id"]["color"]==color)
			ownCarIndex=index;
		else
			{opponentIndices.push_back(index);
			onTrack[cars[index]["id"]["color"].as<std::string>()]=true;}
		}

		
	nextCorner();
	std::cout << "Game initialized" << std::endl;
	return { make_ping() };
}

game_logic::msg_vector game_logic::on_game_start(const jsoncons::json& data)
{
  std::cout << "Race started" << std::endl;
  started=true;
  return { make_ping() };
}

std::string game_logic::decideSwitch(size_t index, size_t current)
	{size_t i=index;

	float angle=0;
	while (i<pieceCount && !pieces[i+1]->isSwitch())
		{i++;
		angle+=pieces[i]->getAngle();}
	if (angle>0 && current<laneCount)
		return "Right";
	if (angle<0 && current>0)
		return "Left";
	return "no";}
	
float game_logic::nextCornerDist()
	{size_t i=currentIndex;
	float dist=pieces[currentIndex]->getLength()-currentPos;
	while (nextCornerIndex<i)
		{i++;
		if (i==pieceCount)
			i=0;
		dist+=pieces[i]->getLength();}
	while (i+1<nextCornerIndex)
		{i++;
		if (i==pieceCount)
			i=0;
		dist+=pieces[i]->getLength();}
	//std::cout << currentIndex << " " << dist << std::endl;
	return dist;}
	
void game_logic::nextCorner()
	{size_t i=currentIndex;
	while(pieces[i]->isStraight())
		{i++;
		if (i==pieceCount)
			i=0;}
	nextCornerIndex=i;	
	cornerAngle=pieces[i]->getAngle();
	float rad=pieces[i]->getRadius();
	float temp=pieces[i+1]->getRadius();
	//std::cout << currentIndex; //" "<<rad << std::endl << " "<<temp<<std::endl;
	while((temp==rad)&&((pieces[i+1]->getAngle() > 0 && cornerAngle > 0)||(pieces[i+1]->getAngle() < 0 && cornerAngle < 0)))
		{i++;
		temp=pieces[i+1]->getRadius();
		//std::cout << " "<< i << " "<< temp << " "<<(temp==rad) << std::endl;
		if (i==pieceCount)
			i=0;
		cornerAngle+=pieces[i]->getAngle();
		}
	if (!pieces[currentIndex]->isStraight())
		nextCornerIndex=i+1;
	while (pieces[nextCornerIndex]->isStraight())
		{nextCornerIndex++;
		if (nextCornerIndex==pieceCount)
			nextCornerIndex=0;}
	if (cornerAngle < 0)
		cornerAngle *= -1;
	//std::cout  << " "<< nextCornerIndex << " " << cornerAngle << std::endl;
	}
	
float game_logic::getTarget(size_t lane, size_t index)
	{float rad=pieces[index]->getRadius();
	if (pieces[index]->getAngle()>0)
		{rad-=lanes[lane];}
	else
		{rad+=lanes[lane];}
	if (deceleration<1.85)
		rad*=deceleration/1.85*100;
	return crashVel*sqrt(rad/90.0);}

game_logic::msg_vector game_logic::on_car_positions(const jsoncons::json& data)
{ 	
	if (data[ownCarIndex]["id"]["color"].as<std::string>()!=color)
		{opponentIndices.push_back(ownCarIndex);
		ownCarIndex=0;
		while(data[ownCarIndex]["id"]["color"].as<std::string>()!=color)
			ownCarIndex++;
		opponentIndices.remove(ownCarIndex);}
	previousAngle=angle;
	angle=data[ownCarIndex]["angle"].as<double>();
	
	
	const jsoncons::json& piecePos= data[ownCarIndex]["piecePosition"];
	previousIndex= currentIndex;
	previousPos  = currentPos;
	currentIndex = piecePos["pieceIndex"].as<size_t>();
	currentPos   = piecePos["inPieceDistance"].as<double>();
	size_t current=piecePos["lane"]["startLaneIndex"].as<size_t>();
	
	if (!started)
		{startIndex=currentIndex;
		if (currentPos!=0)
			startIndex+=1;
		if (startIndex == pieceCount)
			startIndex=0;
		return {make_ping()};}
	if (!allowSwitch && !pieces[currentIndex+1]->isSwitch())
		allowSwitch=true;
	if (allowSwitch && tested && pieces[currentIndex+1]->isSwitch())
		{allowSwitch=false;

		std::string dir=decideSwitch(currentIndex+1, current);
		if (dir != "no")
			return { make_switch(dir) };}
	vel=currentPos-previousPos;
	angularVel=angle-previousAngle;

	if (currentIndex > previousIndex)
		{vel+=pieces[previousIndex]->getLength();
		if (!pieces[previousIndex]->isStraight())
			vel-=lanes[current]*pieces[previousIndex]->getAngle()*2*3.14/360;
		nextCorner();}
		
		
// Speed, angle, angular vel	
//	if (currentIndex < 2 && !tested)
//		std::cout << currentIndex << "s: " << vel << " a: " << angle << " av: " << angularVel << std::endl;
	
	
	
	if (turbo && (currentIndex != turboIndex) && pieces[currentIndex]->isStraight() && (nextCornerDist() > 300))
			{turbo=false;
			return {make_request("turbo", "turbo activated")};}
	if (!tested)
		{if (currentIndex < startIndex)
			return {make_throttle(0.1)};
		if (currentIndex < startIndex+1)
			return {make_throttle(1.0)};
		if (currentIndex < startIndex+2)
			{if (brakeTestVel==0)
				brakeTestVel=vel;
			return {make_throttle(0.0)};}
		if (currentIndex < startIndex +4)
			{deceleration=0.95*(brakeTestVel-vel)/pieces[startIndex+1]->getLength();
			tested=true;
			std::cout << brakeTestVel-vel << std::endl;}
}

	float throttle=1.0;
	//Next corner
	float targetVel=getTarget(current, nextCornerIndex);
	if (pieces[currentIndex]->isStraight() && (cornerAngle < 60.0))
		targetVel *= 60.0/cornerAngle;
	if (abs(angle)>5 && ((angle > 0 && pieces[nextCornerIndex]->getAngle()<0 ) || (angle < 0 && pieces[nextCornerIndex]->getAngle()>0 )))
		targetVel *=0.97;
		
	if (nextCornerDist()*0.97 < (vel-targetVel)/deceleration && vel >targetVel)
		throttle=0.0;
	//Current corner
	targetVel=getTarget(current, currentIndex);
	if ((!pieces[currentIndex]->isStraight())&&(vel>targetVel/1.05))
		throttle=0.45*sqrt(pieces[currentIndex]->getRadius()/100);

		
	//Fail-safes
	if (((angle >0.0 && angularVel >0.0) || (angle <0.0 && angularVel <0.0)) && abs(angle)>(crashAngle-10.0))
		throttle*= (1.0-abs(angle)/crashAngle);
	if (((angularVel>2.5)&& (angle >0.0)) || ((angularVel<-2.5) && (angle<0.0)))
		throttle=0.0;
	if (abs(4*angularVel+angle) > crashAngle)
		throttle=0.0;
		
	//Crash that opponent
	float nextCarDist=1000;
	for(auto opponent=opponentIndices.begin(); opponent!= opponentIndices.end(); opponent++)
		{const jsoncons::json& pos= data[*opponent]["piecePosition"];
		if (pos["lap"].as<size_t>()>=piecePos["lap"].as<size_t>())
			{if ((pos["lane"]["endLaneIndex"].as<size_t>()==current)&& (onTrack[data[*opponent]["id"]["color"].as<std::string>()]))	
				{size_t index=pos["pieceIndex"].as<size_t>();
				size_t i=currentIndex;
				float temp=pos["inPieceDistance"].as<double>()-currentPos;
				if (index >= i)
					{while(index>i)
						{temp+=pieces[i]->getLength();
						i++;
						if (i==pieceCount)
							i=0;}
							
						if ((temp>0) && (temp<nextCarDist))
							nextCarDist=temp;
					}
				}
			}
		}
	if ((nextCarDist < 100)&& pieces[currentIndex]->isStraight())
		throttle=1.0;

	if (throttle>1.0)
		throttle = 1.0;
	if (throttle <0.0)
		throttle = 0.0;
//std::cout << currentIndex << "s: " << vel << " a: " << angle << " av: " << angularVel << " t "<<throttle << std::endl;
	
	return { make_throttle(throttle) };
}

game_logic::msg_vector game_logic::on_crash(const jsoncons::json& data)
{
	if (data["color"].as<std::string>()==color)
	{
	if ((abs(angle)-5)<crashAngle)
		{crashAngle=angle;
		if (crashAngle<0)
			crashAngle*=-1;}
	std::cout << "Crashed at "<< currentIndex <<" with speed " << vel << ", angle " << angle << " and angular velocity " << angularVel <<std::endl;

  return { make_throttle(1.0) };}
  
  std::cout << data["name"].as<std::string>()<< " crashed" << std::endl;
  onTrack[data["color"].as<std::string>()]=false;
	return {make_ping()};
}

game_logic::msg_vector game_logic::on_spawn(const jsoncons::json& data)
{
	if (data["color"].as<std::string>()!=color)
	  onTrack[data["color"].as<std::string>()]=true;
  
  std::cout << data["name"].as<std::string>()<< " spawned" << std::endl;

	return {make_ping()};
}

game_logic::msg_vector game_logic::on_game_end(const jsoncons::json& data)
{
  std::cout << "Race ended" << std::endl;
  return { make_ping() };
}

game_logic::msg_vector game_logic::on_tournament_end(const jsoncons::json& data)
{
  for (auto iter = pieces.begin(); iter!= pieces.end(); iter++)
	delete iter->second;
  std::cout << "Tournament ended" << std::endl;
  return { make_ping() };
}

game_logic::msg_vector game_logic::on_error(const jsoncons::json& data)
{
  std::cout << "Error: " << data.to_string() << std::endl;
  return { make_ping() };
}

game_logic::msg_vector game_logic::on_turbo(const jsoncons::json& data)
{
  turbo=true;
  turboIndex=currentIndex;
  std::cout << "Turbo acquired (" << data["turboDurationMilliseconds"] << "ms, factor " 
  <<data["turboFactor"]<< "), piece "<< currentIndex <<std::endl;
  return { make_ping() };
}
