#ifndef HWO_GAME_LOGIC_H
#define HWO_GAME_LOGIC_H

#include <string>
#include <vector>
#include <map>
#include <list>
#include <functional>
#include <iostream>
#include <jsoncons/json.hpp>
#include "track.h"
#include <cmath>
class game_logic
{
public:
  typedef std::vector<jsoncons::json> msg_vector;

  game_logic();
  msg_vector react(const jsoncons::json& msg);

private:
  typedef std::function<msg_vector(game_logic*, const jsoncons::json&)> action_fun;
  const std::map<std::string, action_fun> action_map;

  msg_vector on_join(const jsoncons::json& data);
  msg_vector on_yourcar(const jsoncons::json& data);
  msg_vector on_game_init(const jsoncons::json& data);
  msg_vector on_game_start(const jsoncons::json& data);
  msg_vector on_car_positions(const jsoncons::json& data);
  msg_vector on_crash(const jsoncons::json& data);
  msg_vector on_spawn(const jsoncons::json& data);
  msg_vector on_game_end(const jsoncons::json& data);
  msg_vector on_tournament_end(const jsoncons::json& data);
  msg_vector on_error(const jsoncons::json& data);
  msg_vector on_turbo(const jsoncons::json& data);
  std::string decideSwitch(size_t index, size_t current);
  std::string color;

  std::map<size_t,TrackPiece*> pieces;
  std::map<size_t, TrackPiece*> corners;
  std::map<size_t, float> lanes;
  size_t pieceCount;
  size_t laneCount;
  size_t ownCarIndex;
  std::list<size_t> opponentIndices;
  bool allowSwitch;
  bool turbo;
  size_t turboIndex;
  
  float currentPos;
  size_t currentIndex;
  float previousPos;
  size_t previousIndex;
  float vel;
  float crashVel;
  float targetVel;
  float brakeDist;
  float getTarget(size_t lane, size_t index);
  float nextCornerDist();
  size_t nextCornerIndex;
  void nextCorner();
  float cornerAngle;
  
  float angle;
  float previousAngle;
  float angularVel;
  float crashAngle;
  
  bool started;
  bool tested;
  size_t startIndex;
  float brakeTestVel;
  float deceleration;
  std::map<std::string, bool> onTrack;
};

#endif
